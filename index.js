import { World } from "./node_modules/ecsy/build/ecsy.module.js";

import { AffichageBesoin } from './src/systems/AffichageBesoin.js';

import { Besoins } from './src/components/Besoins.js';
import { DiminutionBesoin } from "./src/systems/DiminutionBesoin.js";
import { SystemMourir } from "./src/systems/SystemMourir.js";

const world = new World();

world.registerComponent(Besoins);

let miniGotchi = world.createEntity();
miniGotchi.addComponent(Besoins);

world.registerSystem(DiminutionBesoin);
world.registerSystem(SystemMourir);
world.registerSystem(AffichageBesoin);


document.querySelector('#iterate').addEventListener('click', () => {
    world.execute();
});
