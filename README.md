# Minigotchi 

## V1

Le MiniGotchi commence avec ses besoins satisfaits (100pt)

- Faim (Prof)
- Soif (Adama, Jessim, Matiss)
- Sommeil (Folly, Arthur, Mathieu)
- Besoin (Richard, Alix, Thomas)

A chaque itération, chaque besoin baisse d'un montant défini multiplié par une dose d'aléatoire. Utiliser Math.rand()

Utiliser une barre `progress` pour montrer l'évolution du besoin.

## V2 Prendre soin de son minigotchi

Afin de pouvoir prendre soin de son minigotchi, il faut remplir ses besoins. Pour ce faire :

- Créer un bouton (Nourrir, Dormir, etc...)
- Quand on click sur le bouton rajouter + 20pt au besoin concerné

Pour ce faire, ajouter dans votre composant une valeur du type : 
```js
    manger: { type: Types.Boolean, default: false }
```

Ajouter un bouton manger dans l'html, quand on click sur ce bouton :

- Recuperer miniGotchi.getMutableComponent(VotreComposant)
- Mettre votreComposant.manger à true
- Et enfin dans votre système, si cette valeur est à true, remplir la valeur au lieu de la baisser.

## V3 La Mort

Créer un système qui, si un des besoins est inférieur à zéro, affiche une alert et relance la partie de zéro.

## V4 Sauvegarde

Créer un système de sauvegarde qui s'éxécute à chaque itération
- Sauvegarde l'état grace à JSON.stringify(), JSON.parse(), localStorage.setItem(), localStorage.getItem()

## V5 Evenement aléatoire

- Chaque jours, un évènement aléatoire à 1/20 chance d'arriver
  - Soit il est positif, et fige un besoin pendant 5 itérations
  - Soit il est négatif, et augmente la diminution du besoin par x1.2
- En fonction, la barre de progression de ce besoin doit être rouge ou verte

## V6 Score et historique

- A chaque nouvelle partie, on doit demander un nom à l'utilisateur pour le minigotchi (penser au système mourir)
- Chaque itération ajoute un point de score à la partie en cours
- Afficher le score actuel à coté des besoins
- Quand le minigotchi meurt, on ajoute son score et son nom dans un historique
- L'historique est affiché dans une liste à gauche de la page


