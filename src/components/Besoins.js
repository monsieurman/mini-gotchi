import { Component, Types } from '../../node_modules/ecsy/build/ecsy.module.js';

function createBesoins(names) {
    return names.map(([name, action]) =>({ 
        name, 
        action, 
        value: 100, 
        refill: false 
    }));
}

export class Besoins extends Component { }

Besoins.schema = {
    values: {
        type: Types.Array,
        default: createBesoins([
            ['faim', 'Nourrir'],
            ['soif', 'Boire'],
            ['sommeil', 'Dormir'],
            ['besoins', 'Aller au toilette']
        ])
    }
}