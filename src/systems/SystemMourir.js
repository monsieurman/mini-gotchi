import { System } from '../../node_modules/ecsy/build/ecsy.module.js';
import { Besoins } from '../components/Besoins.js';

export class SystemMourir extends System {
    execute() {
        this.queries.needs.results.forEach(entity => {
            let needs = entity.getMutableComponent(Besoins);
            const emptyNeed = needs.values.find(need => need.value <= 0);
            
            if (emptyNeed) {
                alert(`Votre minigotchi est mort de ${emptyNeed.name}, en voila un nouveau`);
                needs.values.forEach(need => need.value = 100);
            }
        });
    }
}

SystemMourir.queries = {
    needs: { components: [Besoins] }
}