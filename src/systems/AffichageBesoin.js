import { System } from '../../node_modules/ecsy/build/ecsy.module.js';
import { Besoins } from '../components/Besoins.js';

export class AffichageBesoin extends System {
    needsNode;

    init() {
        this.needsNode = document.querySelector('.needs');
        this.queries.needs.results.forEach(entity => {
            const needs = entity.getMutableComponent(Besoins);
            console.log(needs);
            const nodes = needs.values.map(need => this.createNeedNode(need, entity));
            nodes.forEach(node => this.needsNode.appendChild(node));
        });
    }

    createNeedNode(need, entity) {
        const needNode = document.createElement('div');
        needNode.classList.add('need');
        needNode.id = need.name;
        needNode.innerHTML = `
        <strong>${need.name}</strong>
        <label for="${need.name}-p">${need.value}%</label>
        <progress id="${need.name}-p" value="${need.value}" max="100"></progress>
        `;
        const button = document.createElement('button');
        button.id = need.name + '-b';
        button.textContent = need.action;
        needNode.appendChild(button);

        button.addEventListener('click', () => {
            need.refill = true;
            this.world.execute();
        });

        return needNode;
    }

    execute() {
        this.queries.needs.results.forEach(entity => {
            let needs = entity.getComponent(Besoins);
            
            needs.values.forEach(need => {
                const needNode = document.querySelector('#' + need.name);
                needNode.querySelector('label').innerText = `${Math.floor(need.value)}%`;
                needNode.querySelector('progress').value = need.value;
            })
        });
    }
}

AffichageBesoin.queries = {
    needs: { components: [Besoins] }
}