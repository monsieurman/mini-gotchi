import { System } from '../../node_modules/ecsy/build/ecsy.module.js';
import { Besoins } from '../components/Besoins.js';

export class DiminutionBesoin extends System {
    execute() {
        this.queries.needs.results.forEach(entity => {
            let needs = entity.getMutableComponent(Besoins);
            
            needs.values.forEach(need => {
                if (need.refill) {
                    need.value = Math.min(100, need.value + 15);
                    need.refill = false;
                } else {
                    need.value = need.value - (5 * Math.random());
                }
            });
        });

    }
}

DiminutionBesoin.queries = {
    needs: { components: [Besoins] }
}